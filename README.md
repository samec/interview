## About

This ansible role was created and tested on own 'debian-test' server using dokerized ansible server. Once all tests passed it was applied on task-for-candidate-2 using same remote ansible server.
Final result is a complete rabbitMQ installation with basic send/receive message testing. Admin console will be availible on port 15672 for user 'test'  

syntax compliance check:
```
root@ansible:~/playbooks# yamllint .
./roles/rabbitmq/vars/vault.yml
  1:1       warning  missing document start "---"  (document-start)
```

## Prerequisites:
* git clone https://gitlab.com/samec/interview
* Root ssh keys are installed on client server (sudo method was not used)
* Connection between ansible server and target server is OK:
```
# ansible all -m ping

debian-test | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
task-for-candidate-2 | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

* All required credentials are know like ansible vault password
* role can be assigned either to 'rabbitmq-test' or to 'rabbitmq-prod' server group
```
root@ansible:~/playbooks# cat /etc/ansible/hosts
[rabbitmq-test]
debian-test

[rabbitmq-prod]
task-for-candidate-2

root@ansible:~/playbooks# grep -iP "ansible|debian-test|task-for-candidate-2" /etc/hosts
172.29.4.2	ansible
192.168.40.17   debian-test
xxx.xx.xx.xx	task-for-candidate-2

root@ansible:~/playbooks# cat runsetup.yml
---
- hosts: rabbitmq-prod
  gather_facts: "yes"
  become_user: root
  roles:
    - rabbitmq
```

After deployment you should be able to login to admin GUI via http://<host_name>:15672 with test/admin credentials. There are two sample scripts to make sure producer and consumer exchange is working: /root/receive.py, /root/send.py

## Usage of this ansible playbook(role) :

```
root@ansible:~/playbooks# ansible-playbook runsetup.yml --ask-vault-pass
Vault password:
```

## Expected sample result:
```
**root@ansible:~/playbooks# ansible-playbook runsetup.yml --ask-vault-pass**
Vault password:

PLAY [rabbitmq-prod] ***************************************************************************************************************************************************************************************

TASK [Gathering Facts] *************************************************************************************************************************************************************************************
ok: [task-for-candidate-2]

TASK [rabbitmq : include_vars] *****************************************************************************************************************************************************************************
ok: [task-for-candidate-2]

TASK [rabbitmq : Install apt-get https] ********************************************************************************************************************************************************************
ok: [task-for-candidate-2]

TASK [rabbitmq : Install RabbitMQ public key] **************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Add RabbitMQ repo] ************************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Add Erlang offical repo] ******************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Update all packages] **********************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Install RabbitMQ package] *****************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Enable admin plugin] **********************************************************************************************************************************************************************
changed: [task-for-candidate-2]

RUNNING HANDLER [rabbitmq : restart rabbitmq] **************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Remove guest] *****************************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Create vhost] *****************************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Create rabbitmq systemd service config dir] ***********************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Set openfiles limit to 100K] **************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : reload systemd] ***************************************************************************************************************************************************************************
ok: [task-for-candidate-2]

TASK [rabbitmq : rabbitmq_user] ****************************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Firewall rule to drop traffic on 5672] ****************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Firewall rule to allow traffic on 15672] **************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Install apt-get python-pika] **************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : transfer send.py] *************************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : transfer receive.py] **********************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Sending test messages to test queue] ******************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Checking log result] **********************************************************************************************************************************************************************
changed: [task-for-candidate-2]

TASK [rabbitmq : Return only the shell standard output] ****************************************************************************************************************************************************
ok: [task-for-candidate-2] => {
    "msg": [
        "declaring queue with TTL 3600.",
        "binding test exchange to test queue.",
        " [*] Waiting for messages. To exit press CTRL+C",
        " [x] OK Received 'Hello Tesonet!'",
        " [x] OK Received 'Hello Tesonet!'",
        " [x] OK Received 'Hello Tesonet!'"
    ]
}

PLAY RECAP *************************************************************************************************************************************************************************************************
task-for-candidate-2       : ok=25   changed=20   unreachable=0    failed=0
```